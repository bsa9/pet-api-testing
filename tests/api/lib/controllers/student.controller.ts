import { ApiRequest } from '../request';

let baseUrl: string = 'https://knewless.tk/api/';

export class StudentController {
	
    async getSettings(accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method('GET')
            .url(`student`)
            .bearerToken(accessToken)
            .send();
        return response;
    }

    async setSettings(accessToken: string, studentSettings: any) {
        const response = await new ApiRequest()
			.prefixUrl(baseUrl)
            .method('POST')
            .url(`student`)
            .bearerToken(accessToken)
            .body(studentSettings)
            .send();
        return response;
    }

    async getGoals(accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method('GET')
            .url(`goals`)
            .bearerToken(accessToken)
            .send();
        return response;
    }    

    async setGoal(accessToken: string, goal: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method('POST')
            .url(`student/goal`)
            .bearerToken(accessToken)
            .body({goalId: goal})
            .send();
        return response;
    }

    async getCurrentProgress(accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method('GET')
            .url(`student/goal`)
            .bearerToken(accessToken)
            .send();
        return response;
    }

    async getStudentData(accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method('GET')
            .url(`student/info`)
            .bearerToken(accessToken)
            .send();
        return response;
    }

    async getStudentProfile(accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method('GET')
            .url(`student/profile`)
            .bearerToken(accessToken)
            .send();
        return response;
    }
	
}