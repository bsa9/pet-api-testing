import { ApiRequest } from "../request";

let baseUrl: string = 'https://knewless.tk/api/';

export class CoursesController {
	
    async getAllCourses() {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url(`course/all`) 
            .send();
        return response;
    }

    async getPopularCourses() {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url(`course/popular`)
            .send();
        return response;
    }

    async getCourseInfoById(id: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url(`course?id=${id}`)
            .send();
        return response;
    }
	
}