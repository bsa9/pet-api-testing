import { expect } from 'chai';
import { AuthController } from '../lib/controllers/auth.controller';
import { UserController } from '../lib/controllers/user.controller';
import { StudentController } from '../lib/controllers/student.controller';
import { FavoritesController } from '../lib/controllers/favorites.controller';

const schemas = require('./data/schemas.json');
var chai = require('chai');
chai.use(require('chai-json-schema'));

const auth = new AuthController();
const user = new UserController();
const student = new StudentController();
const favorites = new FavoritesController();

describe('Student auto tests', () => {

    let accessToken: string, studentId: string, goalId: string;
	let userInfo: any, studentSettings: any, studentGoal: any, studentData: any, studentProfile: any, favoriteCourses: any;

    it(`Student Login`, async () => {
        let response = await auth.authenticateUser('petro.getman@i.ua', 'jdurdPAss23fskwe');
        accessToken = response.body.accessToken;
        expect(response.statusCode, `Status Code should be 200`).to.equal(200);
		expect(response.timings.phases.total, `Response time should be less than 3s`).to.be.lessThan(3000);
        expect(response.body).to.be.jsonSchema(schemas.schema_login_response);
		//console.log(accessToken);
    });

    it(`Get user info`, async () => {
        let response = await user.getCurrentUser(accessToken);
        userInfo = response.body;
        expect(response.statusCode, `Status Code should be 200`).to.equal(200);
		expect(response.timings.phases.total, `Response time should be less than 3s`).to.be.lessThan(3000);
        expect(response.body).to.be.jsonSchema(schemas.schema_userInfo);
		//console.log(userInfo);
    });
	
    it(`Get Student settings`, async () => {
        let response = await student.getSettings(accessToken);
        studentSettings = response.body;
		studentId = response.body.id;
        expect(response.statusCode, `Status Code should be 200`).to.equal(200);
		expect(response.timings.phases.total, `Response time should be less than 3s`).to.be.lessThan(3000);		
        expect(response.body).to.be.jsonSchema(schemas.schema_studentSettings);
		//console.log(studentSettings);
    });
	
    it(`Update Student settings`, async () => {
		
        studentSettings.job = 'Updated BSA Student';
		studentSettings.company = 'Updated BSA';
		studentSettings.location = 'Poland';
		studentSettings.website = 'abc.com';

		let response = await student.setSettings(accessToken, studentSettings);
        expect(response.statusCode, `Status Code should be 200`).to.equal(200);
		expect(response.timings.phases.total, `Response time should be less than 3s`).to.be.lessThan(3000);
        expect(response.body).to.be.jsonSchema(schemas.schema_update_settings);        
    });

    it(`Get Student settings`, async () => {
        let response = await student.getSettings(accessToken);
        studentSettings = response.body;
        expect(response.statusCode, `Status Code should be 200`).to.equal(200);
		expect(response.timings.phases.total, `Response time should be less than 3s`).to.be.lessThan(3000);
        expect(response.body).to.be.jsonSchema(schemas.schema_studentSettings);
		console.log('Student settings updated');
		//console.log(studentSettings);
    });	
    
    it(`Get goals`, async () => {
        let response = await student.getGoals(accessToken);
        goalId = response.body[0].id;
        expect(response.statusCode, `Status Code should be 200`).to.equal(200);
		expect(response.timings.phases.total, `Response time should be less than 3s`).to.be.lessThan(3000);
        expect(response.body).to.be.jsonSchema(schemas.schema_goals);
		//console.log(studentGoal);
    });

    it(`Set Student goal`, async () => {
        let response = await student.setGoal(accessToken, goalId);
        expect(response.statusCode, `Status Code should be 200`).to.equal(200);
		expect(response.timings.phases.total, `Response time should be less than 3s`).to.be.lessThan(3000);
		//console.log(studentGoal);
    });
    

    it(`Get Student progress (goal)`, async () => {
        let response = await student.getCurrentProgress(accessToken);
        studentGoal = response.body;
        expect(response.statusCode, `Status Code should be 200`).to.equal(200);
		expect(response.timings.phases.total, `Response time should be less than 3s`).to.be.lessThan(3000);
        expect(response.body).to.be.jsonSchema(schemas.schema_student_goal);
		//console.log(studentGoal);
    });	
		
    it(`Get Student data`, async () => {
        let response = await student.getStudentData(accessToken);
		studentData = response.body;
        expect(response.statusCode, `Status Code should be 200`).to.equal(200);
		expect(response.timings.phases.total, `Response time should be less than 3s`).to.be.lessThan(3000);
        expect(response.body).to.be.jsonSchema(schemas.schema_student_data);
		expect(response.body.id).to.be.equal(studentId);
		//console.log(studentData);
    });

    it(`Get Student profile`, async () => {
        let response = await student.getStudentProfile(accessToken);
        studentProfile = response.body;
        expect(response.statusCode, `Status Code should be 200`).to.equal(200);
		expect(response.timings.phases.total, `Response time should be less than 3s`).to.be.lessThan(3000);
        expect(response.body).to.be.jsonSchema(schemas.schema_student_profile);
		//console.log(studentProfile);
    });

    it(`Get Favorite Courses`, async () => {
        let response = await favorites.getFavoriteCourses(accessToken);
        favoriteCourses = response.body;
        expect(response.statusCode, `Status Code should be 200`).to.equal(200);
		expect(response.timings.phases.total, `Response time should be less than 3s`).to.be.lessThan(3000);
        expect(response.body).to.be.jsonSchema(schemas.schema_favorite_courses);
		//console.log(favoriteCourses);
    });
	
	//negative

    it(`Student Login with incorrect password`, async () => {
        let response = await auth.authenticateUser('petro.getman@i.ua', 'durdPAss23fskwe');
        accessToken = response.body.accessToken;
        expect(response.statusCode, `Status Code should be 401`).to.equal(401);
		expect(response.timings.phases.total, `Response time should be less than 3s`).to.be.lessThan(3000);
        //expect(response.body).to.be.jsonSchema(schemas.schema_login_response);
		//console.log(accessToken);
    });

    it(`Student Login with invalid emeil`, async () => {
        let response = await auth.authenticateUser('vetro.getman@i.ua', 'jdurdPAss23fskwe');
        accessToken = response.body.accessToken;
        expect(response.statusCode, `Status Code should be 401`).to.equal(401);
		expect(response.timings.phases.total, `Response time should be less than 3s`).to.be.lessThan(3000);
        //expect(response.body).to.be.jsonSchema(schemas.schema_login_response);
		//console.log(accessToken);
    });
    
    it(`Update Student settings empty values`, async () => {
		
        studentSettings.job = '';
		studentSettings.company = '';
		studentSettings.location = '';
		studentSettings.website = '';

		let response = await student.setSettings(accessToken, studentSettings);
        expect(response.statusCode, `Status Code should be 401`).to.equal(401);
		expect(response.timings.phases.total, `Response time should be less than 3s`).to.be.lessThan(3000);
    });

    
})
