import { expect } from "chai";
import { CoursesController } from '../lib/controllers/courses.controller';

const courses = new CoursesController();

const schemas = require('./data/schemas.json');
var chai = require('chai');
chai.use(require('chai-json-schema'));

describe("Courses controller", () => {
    let courseId: string;

    it(`getAllCourses`, async () => {
        let response = await courses.getAllCourses();
        courseId = response.body[9].id;
        expect(response.statusCode, `Status Code should be 200`).to.be.equal(200);
        expect(response.timings.phases.total, `Response time should be less than 3s`).to.be.lessThan(3000);
        expect(response.body.length).to.be.greaterThan(1);
 
        //console.log(response.body[9]);
        //console.log(response.body[9].id);
    });

    it(`getPopularCourses`, async () => {
        let response = await courses.getPopularCourses();

        // console.log(response.body);

        expect(response.statusCode, `Status Code should be 200`).to.be.equal(200);
        expect(response.timings.phases.total, `Response time should be less than 3s`).to.be.lessThan(3000);
        expect(response.body.length).to.be.greaterThan(1);
    });
    
    it(`getCourseDetails`, async () => {
        let response = await courses.getCourseInfoById(courseId);

        //console.log(response.body);
        
        expect(response.statusCode, `Status Code should be 200`).to.be.equal(200);
        expect(response.timings.phases.total, `Response time should be less than 3s`).to.be.lessThan(3000);
        expect(response.body).to.be.jsonSchema(schemas.schema_courseInfo); 
    });
});
