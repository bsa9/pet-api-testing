import { expect } from 'chai';
import { AuthController } from '../lib/controllers/auth.controller';
import { ArticlesController } from '../lib/controllers/articles.controller';
import { UserController } from '../lib/controllers/user.controller';
import { AuthorController } from '../lib/controllers/author.controller';

const schemas = require('./data/schemas.json');
var chai = require('chai');
chai.use(require('chai-json-schema'));

const auth = new AuthController();
const articles = new ArticlesController();
const user = new UserController();
const author = new AuthorController();

describe('Author auto tests', () => {
    let accessToken: string, userId: string, authorId: string, articleId: string;
	let authorSettings: any, responseSettings: any;
    let articlesCounter: number;

    it(`Author Login`, async () => {
        let response = await auth.authenticateUser('petro.vaskovskyi@yahoo.com', 'qwerty123456');
        accessToken = response.body.accessToken;
        expect(response.statusCode, `Status Code should be 200`).to.equal(200);
		expect(response.timings.phases.total, `Response time should be less than 3s`).to.be.lessThan(3000);
        expect(response.body).to.be.jsonSchema(schemas.schema_login_response);
    });

    it(`Get author settings`, async () => {
        let response = await author.getSettings(accessToken);
        responseSettings = response;
        authorSettings = response.body;
        authorId = response.body.id;        
        expect(response.statusCode, `Status Code should be 200`).to.equal(200);
        expect(response.timings.phases.total, `Response time should be less than 3s`).to.be.lessThan(3000);
        expect(response.body).to.be.jsonSchema(schemas.schema_get_settings_author);
		//console.log(authorSettings);
    });

    it(`Update author settings`, async () => {
        authorSettings.job = 'Updated BSA Author';
		authorSettings.company = 'Updated BSA';
		authorSettings.location = 'Poland';
		
		await author.setSettings(accessToken, authorSettings);
		expect(responseSettings.statusCode, `Status Code should be 200`).to.equal(200);
		expect(responseSettings.timings.phases.total, `Response time should be less than 3s`).to.be.lessThan(3000);
        expect(responseSettings.body).to.be.jsonSchema(schemas.schema_get_settings_author);
    });	

    it(`Get user details`, async () => {
        let response = await user.getCurrentUser(accessToken);
        expect(response.statusCode, `Status Code should be 200`).to.equal(200);
		expect(response.timings.phases.total, `Response time should be less than 3s`).to.be.lessThan(3000);
        expect(response.body).to.be.jsonSchema(schemas.schema_getcurrentuser);
        //console.log (authorSettings);
    });

    it(`Get Public Author`, async () => {
        let response = await author.getPublicAuthor(accessToken, authorId);
        expect(response.statusCode, `Status Code should be 200`).to.equal(200);
		expect(response.timings.phases.total, `Response time should be less than 3s`).to.be.lessThan(3000);
        expect(response.body).to.be.jsonSchema(schemas.schema_getpublicauthor);
    });

    it(`Get articles`, async () => {
        let response = await articles.getArticles(accessToken);
        authorId = response.body.authorId;
        articlesCounter = response.body.length;
        expect(response.statusCode, `Status Code should be 200`).to.equal(200);
		expect(response.timings.phases.total, `Response time should be less than 3s`).to.be.lessThan(3000);
        expect(response.body).to.be.jsonSchema(schemas.schema_get_articles);
        //console.log(response.body.length);
    });

    it(`Add article`, async () => {
        let newArticle = {
            name: 'Test article',
            image: 'https://knewless.tk/assets/images/35dcffdf-ed92-4d94-aa32-67ea0f8afe39.JPG',
            text: '<p>Text of test article</p>\n',
            uploadImage: null
        };

        let response = await articles.saveArticle(accessToken, newArticle);
        articlesCounter += 1;
        articleId = response.body.id;
        expect(response.statusCode, `Status Code should be 200`).to.equal(200);
		expect(response.timings.phases.total, `Response time should be less than 3s`).to.be.lessThan(3000);
        expect(response.body).to.be.jsonSchema(schemas.schema_post_article);
    });

    it(`Get articles after adding new one`, async () => {
        let response = await articles.getArticles(accessToken);
        expect(response.statusCode, `Status Code should be 200`).to.equal(200);
		expect(response.timings.phases.total, `Response time should be less than 3s`).to.be.lessThan(3000);        
        expect(response.body.length).to.be.equal(articlesCounter);
        expect(response.body).to.be.jsonSchema(schemas.schema_get_articles);
        //console.log(response.body.length);
    });

    it(`Get article by ID`, async () => {
        let response = await articles.getArticleById(accessToken, articleId);
        expect(response.statusCode, `Status Code should be 200`).to.equal(200);
		expect(response.timings.phases.total, `Response time should be less than 3s`).to.be.lessThan(3000);        
        expect(response.body).to.be.jsonSchema(schemas.schema_article_by_id);
        //console.log(response.body);
    });

    it(`Add comment to article`, async () => {
        let response = await articles.addComment(accessToken, articleId, 'Some text in test comment');
        expect(response.statusCode, `Status Code should be 200`).to.equal(200);
		expect(response.timings.phases.total, `Response time should be less than 3s`).to.be.lessThan(3000);
        expect(response.body).to.be.jsonSchema(schemas.schema_add_comment_to_article);
    });
    
    it(`Get comments from article by pieces (limited by 200 items)`, async () => {
        let response = await articles.getArticleComment(accessToken, articleId, 200);
        expect(response.statusCode, `Status Code should be 200`).to.equal(200);
		expect(response.timings.phases.total, `Response time should be less than 3s`).to.be.lessThan(3000);
        expect(response.body).to.be.jsonSchema(schemas.schema_article_comments);
    });

    //negative
	
    it(`Author Login invalid email`, async () => {
        let response = await auth.authenticateUser('kotenia2012@gmail.com', 'qwerty123456');
        accessToken = response.body.accessToken;
        expect(response.statusCode, `Status Code should be 401`).to.equal(401);
		expect(response.timings.phases.total, `Response time should be less than 3s`).to.be.lessThan(3000);
    });
	
    it(`Author Login invalid password `, async () => {
        let response = await auth.authenticateUser('kotenia2022@gmail.com', 'qwerty1234567');
        accessToken = response.body.accessToken;
        expect(response.statusCode, `Status Code should be 401`).to.equal(401);
		expect(response.timings.phases.total, `Response time should be less than 3s`).to.be.lessThan(3000);
    });
	
    it(`Update author settings invalid location data (symbols)`, async () => {
		let response = await author.getSettings(accessToken);
        authorSettings.job = 'Updated BSA Author';
		authorSettings.company = 'Updated BSA';
		authorSettings.location = '!@#$%^&*()';
        await author.setSettings(accessToken, authorSettings);
		expect(response.statusCode, `Status Code should be 401`).to.equal(401);
		expect(response.timings.phases.total, `Response time should be less than 3s`).to.be.lessThan(3000);
    });

});
